/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.models.interfaces;



import iut.project.mushroomwar.models.map.Map;



/**
 * Cette interface permet de faire le lien entre un objet de classe {@link Map} et un objet de classe souhaitant le modifier
 * @author Joseph BRIGUET
 * @version 1.0
 */
public interface MapDesigner {
    
    
    
//METHODES PUBLICS
    /**
     * Peinds les champignons
     * @param g Correspond au graphique qui sera peint
     */
    public void paintMushrooms(java.awt.Graphics g);
    
    /**
     * Peinds le nombre d'unité par champignons
     * @param g Correspond au graphique qui sera peint
     */
    public void paintMushroomLabels(java.awt.Graphics g);
    
    /**
     * Peinds la sélection d'un champignons
     * @param g Correspond au graphique qui sera peint
     */
    public void paintMushroomSelected(java.awt.Graphics g);
    
    /**
     * Peinds la sélection d'un champignons
     * @param g Correspond au graphique qui sera peint
     */
    public void paintMovement(java.awt.Graphics g);
    
    /**
     * Peint le compte à rebours
     * @param map Correspond à l'objet {@link Map} qui va être peint
     * @param g Correspond au graphique qui sera peint
     */
    public void paintCountdown(Map map, java.awt.Graphics g);
    
    
    
}