
package iut.project.mushroomwar.models.interfaces;

import iut.project.mushroomwar.models.map.Mushroom;
import iut.project.mushroomwar.models.player.Player;

/**
 *
 * @author Elliot
 */
public interface MapMouse {
    public void mushroomMouseEntered(Player human,Mushroom mushroom, int xPos, int yPos);
    
    public void mushroomMouseExited(Player human,Mushroom mushroom, int xPos, int yPos);
    
    public void mushroomMouseClicked(Player human,Mushroom mushroom, int xPos, int yPos, boolean reclick, boolean left, boolean right);
}
