package iut.project.mushroomwar.models;

import iut.project.mushroomwar.Pulsator;
import iut.project.mushroomwar.assets.Drawer;
import iut.project.mushroomwar.assets.Sound;
import iut.project.mushroomwar.assets.SoundPlayer;
import iut.project.mushroomwar.models.interfaces.MapDesigner;
import iut.project.mushroomwar.models.interfaces.MapMouse;
import iut.project.mushroomwar.models.map.Map;
import iut.project.mushroomwar.models.map.Mushroom;
import iut.project.mushroomwar.models.player.Human;
import iut.project.mushroomwar.models.player.IA;
import iut.project.mushroomwar.models.player.Player;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Elliot
 */
public class Engine extends Pulsator{
    
    private final List<Mushroom> mushrooms;
    private Map map;
    private Player human;
    private Color neutralColor;
    private final List<Player> players;
    private int countdownGameStarting;
    private boolean gameStarted;
    private boolean gameFinished;
    private MapMouse listener;
    private Mushroom entered;

    public Engine(Human human,Map map,List<Player> players){
        this(human,map,new Color(141,141,141),players);
    }
    
    public Engine(Human human,Map map,Color neutralColor,List<Player> players){
        this.mushrooms = new ArrayList<>();
        this.neutralColor = neutralColor;
        this.players = players;
        this.human = human;
        this.map = map;
        this.map.setMouseListener(new MouseAdapter(){
            @Override
            public void mouseMoved(MouseEvent e) {
                Engine.this.mouseMoved(e); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                Engine.this.mouseClicked(e);  // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
            }
            
        });
        
        this.map.setDesignerListener(new MapDesigner(){
            @Override
            public void paintMushrooms(Graphics g) {
                Engine.this.paintMushrooms(g);
            }

            @Override
            public void paintMushroomLabels(Graphics g) {
                Engine.this.paintMushroomLabels(g);
            }

            @Override
            public void paintMushroomSelected(Graphics g) {
                Engine.this.paintMushroomSelected(g);
            }

            @Override
            public void paintMovement(Graphics g) {
                Engine.this.paintMovement(g);
            }

            @Override
            public void paintCountdown(Map map, Graphics g) {
                Engine.this.paintCountdown(Engine.this.map,g);
            }
        });
        
        this.countdownGameStarting = 5;
        
    }

    public void setListener(MapMouse listener) {
        this.listener = listener;
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    public void setGameStarted(boolean gameStarted) {
        this.gameStarted = gameStarted;
    }

    public boolean isGameFinished() {
        return gameFinished;
    }

    public void setGameFinished(boolean gameFinished) {
        this.gameFinished = gameFinished;
    }
    
    
    
    public void addMushroom(Mushroom m){
        if(m != null){
            mushrooms.add(m);
        }
        
    }
    
    public void selectMushroom(Mushroom m){
        unselectAllMushrooms();
        if(m != null){
            m.setSelected(true);
        }
    }
    
    public void unselectMushroom(Mushroom m){
        if(m != null){
            m.setSelected(false);
        }
    }
    
    public void unselectAllMushrooms(){
        for(Mushroom m : this.mushrooms){
            unselectMushroom(m);
        }
    }
    
    public Mushroom getSelectedMushroom(){
        for(Mushroom m : this.mushrooms){
            if(m.isSelected()){
                return m;
            }
        }
        return null;
    }

    @Override
    protected void tick(long cpt, int tps) {
        if(isGameStarted()){
            if(cpt % 100 == 0){
                for(Mushroom m : this.mushrooms){
                    if(!m.isEmpty()){
                        m.addUnit();
                    }
                }
            }
            for(Player player : this.players){
                if(player != null && player instanceof IA){
                    IA ia = (IA)player;
                    ia.observe(this,this.mushrooms);
                }
            }
            this.map.repaint();
        }else{
            if(cpt % 100 == 0){
                this.map.repaint();
            }
        }
    }
    
    private void paintMushrooms(Graphics g){
        for(Mushroom m : this.mushrooms){
            Drawer.paintMushroom(g, m.getPosition().x, m.getPosition().y, m.getTeamColor() == null ? this.neutralColor : m.getTeamColor());
        }
    }
    
    private void paintMushroomLabels(Graphics g) {
        if(isGameStarted()){
            for(Mushroom m : this.mushrooms){
                Drawer.paintLabelMushroom(g, m.getPosition().x, m.getPosition().y, m.countUnits());
            }
        }
        
    }
    
    private void paintMushroomSelected(Graphics g) {
        if(isGameStarted()){
            Mushroom m = getSelectedMushroom();
            if(m != null){
                g.setColor(Color.ORANGE);
                g.fillOval(m.getPosition().x, m.getPosition().y + 64 - 19, 64, 20);
            }
        }
    }
    
    private void paintMovement(Graphics g) {
        
    }
    
    public void paintCountdown(Map map, Graphics g) {
        if(!isGameStarted() && !isGameFinished()){
            this.countdownGameStarting--;
            Drawer.paintCounterdown(g, map.getWidth(), map.getHeight(),this.countdownGameStarting , map.getFont(), map.getForeground());
            if(this.countdownGameStarting == 0){
                setGameStarted(true);
                SoundPlayer.sound(Sound.READY_GO);
            }
        }
        
    }
    
    private void mouseMoved(MouseEvent e){
        if(isGameStarted()){
            int x = e.getX();
            int y = e.getY();
            
            boolean pass = true;
            for(Mushroom m : this.mushrooms){
                if(m.getPosition().x <= x && x < m.getPosition().x + 64 && m.getPosition().y <= y && y < m.getPosition().y + 64){
                    this.entered = m;
                    if(this.listener != null){
                        this.listener.mushroomMouseEntered(human, m, x, y);
                        pass = false;
                    }
                }
            }
            if(pass && this.entered != null ){
                if(this.listener != null){
                    this.listener.mushroomMouseExited(human, this.entered, x, y);
                    this.entered = null;
                }
            }
        }
    }
    
    private void mouseClicked(MouseEvent e){
        if(isGameStarted()){
            int x = e.getX();
            int y = e.getY();
            
            for(Mushroom m: mushrooms){
                if(m.getPosition().x <= x && x < m.getPosition().x + 64 && m.getPosition().y <= y && y < m.getPosition().y  + 64){
                    listener.mushroomMouseClicked(human, m, x, y, m.isSelected(), e.getButton() == 1, e.getButton() == 3);
                }
            }
        }
    }
}
