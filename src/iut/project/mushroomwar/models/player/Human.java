/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.models.player;



/**
 * Cette classe représente les objets "Joueur humain"
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class Human extends Player {

    
    
//CONSTRUCTORS
    /**
     * Crée un humain
     */
    public Human() {
    }

    /**
     * Crée un humain
     * @param firstname Correspond au prénom de l'humain
     * @param color Correspond à la couleur de l'humain
     */
    public Human(String firstname, java.awt.Color color) {
        super(firstname, color);
    }
    
    
    
}