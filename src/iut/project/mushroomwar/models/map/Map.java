/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.models.map;



import iut.project.mushroomwar.assets.Drawer;
import iut.project.mushroomwar.models.interfaces.MapDesigner;



/**
 * Cette classe représente les objets "arrière plan" d'une map de jeu
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class Map extends javax.swing.JPanel {
    
    
    
//ATTRIBUTS
    /**
     * Correspond à l'image de la map de jeu
     */
    private final java.awt.Image background;
    
    /**
     * Correspond aux dimensions de l'objet {@link Map}
     */
    private final java.awt.Dimension dimension;
    
    /**
     * Correspond à l'objet écouteur qui sera informé de certains évênements sur cet objet {@link Map}
     */
    private java.awt.event.MouseAdapter mouseListener;
    
    /**
     * Correspond à l'objet écouteur qui sera informé de certains évênements sur cet objet {@link Map}
     */
    private MapDesigner designerListener;

    
    
//CONSTRUCTOR
    /**
     * Crée un objet {@link Map}
     */
    public Map() {
        this.background = Drawer.getBackgroundMap();
        
        //Retaille le composant
        this.dimension = new java.awt.Dimension(background.getWidth(null), background.getHeight(null));
        super.setSize(this.dimension);
        super.setPreferredSize(this.dimension);
        super.setMinimumSize(this.dimension);
        super.setMaximumSize(this.dimension);
        
        super.addMouseMotionListener(new java.awt.event.MouseAdapter() {
            
            @Override
            public void mouseMoved(java.awt.event.MouseEvent e) {
                if(mouseListener != null)
                    mouseListener.mouseMoved(e);
            }
            
        });
        super.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent e) {
                if(mouseListener != null)
                    mouseListener.mouseClicked(e);
            }
        });
    }

    
    
//METHODES PUBLICS
    /**
     * Assigne un objet qui sera mis au courant des actions de la souris sur l'objet {@link Map}
     * @param mouseListener Correspond au listener qui sera mis au courant des actions de la souris sur l'objet {@link Map}
     */
    public final void setMouseListener(java.awt.event.MouseAdapter mouseListener) {
        this.mouseListener = mouseListener;
    }

    /**
     * Assigne un objet qui sera mis au courant des différentes étapes de dessin
     * @param designerListener Correspond au listener qui sera mis au courant des différentes étapes de dessin
     */
    public final void setDesignerListener(MapDesigner designerListener) {
        this.designerListener = designerListener;
    }
    
    /**
     * Reinitialise la carte. Celle-ci ne fait apparaître que la carte sans les unités ou les champignons. La carte n'est plus lié à un {@link Engine}
     */
    public final void reset(){
        this.designerListener = null;
        this.mouseListener = null;
        repaint();
    }
    
    
    
//METHODE PROTECTED
    /**
     * Repeint l'objet {@link Map}
     * @param g Correspond au graphique (ou toile) de l'objet {@link Map} à repeindre
     */
    @Override
    protected final void paintComponent(java.awt.Graphics g){
        super.paintComponent(g);
        g.drawImage(background, 0, 0, null);
        
        java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;

        g2d.setRenderingHint(
                java.awt.RenderingHints.KEY_ANTIALIASING,
                java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(
                java.awt.RenderingHints.KEY_TEXT_ANTIALIASING,
                java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        if(this.designerListener != null){
            this.designerListener.paintMushroomSelected(g);
            this.designerListener.paintMushrooms(g);
            this.designerListener.paintMushroomLabels(g);
            this.designerListener.paintMovement(g);
            this.designerListener.paintCountdown(this, g);
        }
    }
    
    
    
}