/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar.assets;



/**
 * Cette classe énumère les différents sons brefs possibles
 * @author Joseph BRIGUET
 * @version 1.0
 */
public enum Sound {
    
    
    
//CONSTANTES
    /**
     * Lorsque le joueur humain a gagné
     */
    YOU_WON("you_won.wav", 1.0f),
    
    /**
     * Lorsque le joueur humain a perdu
     */
    YOU_LOST("you_lost.wav", 1.0f),
    
    /**
     * Lorsqu'un champignon a été capturé par un autre joueur
     */
    CAPTURED_MUSHROOM("cartoon.wav", 0.75f),
    
    /**
     * Lorsque la partie peut commencer (après le compte à rebours)
     */
    READY_GO("horn.wav", 0.8f);
    
    
    
//ATTRIBUTS
    /**
     * Correspond au nom du fichier sonore
     */
    private final String file;
    
    /**
     * Correspond au niveau du volume sonore
     */
    private final float volume;

    
    
    /**
     * Crée un son
     * @param file Correspond au nom du fichier sonore
     * @param volume Correspond au niveau du volume sonore
     */
    private Sound(String file, float volume) {
        this.file = file;
        this.volume = volume;
    }

    
    
//METHODES PUBLICS
    /**
     * Renvoie le volume autorisé pour ce son
     * @return Retourne le volume autorisé pour ce son
     */
    public final float volume() {
        return volume;
    }
    
    /**
     * Renvoie le son sous la forme d'une chaîne de caractères
     * @return Retourne le son sous la forme d'une chaîne de caractères
     */
    @Override
    public final String toString() {
        return file;
    }
    
    
    
}