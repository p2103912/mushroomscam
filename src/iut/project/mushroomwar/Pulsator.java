/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar;



/**
 * Cette classe permet de créer un objet qui génèrera une pulsation toutes les n millisecondes. 
 * L'objet adaptera sa pulsation pour rester le plus régulier possible en tenant compte du temps d'exécution du code utilisateur pour chaque tick
 * @author Joseph BRIGUET
 * @version 1.0
 */
public abstract class Pulsator implements Comparable<Pulsator> {

    
    
//ATTRIBUT STATIC
    /**
     * Correspond au compteur de création des pulsateurs
     */
    private static int counterID = 0;
    
    
    
//ATTRIBUTS
    /**
     * Correspond à l'id de ce pulsateur
     */
    private final int id;
    
    /**
     * Toutes les n millisecondes, il y a un nouveau tick. {@link #durationTick} correspond à n
     */
    private final long durationTick;
    
    /**
     * Chaque tick est décomposé en sous-ticks. Toutes les n millisecondes, il y a un nouveau sous-tick. {@link #countLoopByTick} correspond à n
     */
    private final long countLoopByTick;
    
    /**
     * Correspond au thread qui sera en charge de la pulsation
     */
    private final Thread thread;
    
    /**
     * Détermine si l'objet {@link Pulsator} est en fonction ou pas
     */
    private boolean run;
    
    
    
//CONSTRUCTORS
    /**
     * Crée un objet {@link Pulsator}
     */
    public Pulsator() {
        this(8, 4);
    }
    
    /**
     * Crée un objet {@link Pulsator}
     * @param durationTick Toutes les n millisecondes, il y a un nouveau tick. durationTick correspond à n
     * @param durationSubTick Chaque tick est décomposé en sous-ticks. Toutes les n millisecondes, il y a un nouveau sousstick. durationSubTick correspond à n
     */
    public Pulsator(long durationTick, long durationSubTick) {
        this.id                 = counterID++;
        this.durationTick       = durationTick;
        this.countLoopByTick    = durationSubTick;
        this.thread             = new Thread(createRunnable());
        this.thread.setPriority(Thread.MAX_PRIORITY);
    }
    
    
    
//METHODES PUBLICS
    /**
     * Détermine si le pulsateur est en marche ou pas
     * @return Retourne true s'il l'est, sinon false
     */
    public final boolean isStarted(){
        return this.run;
    }
    
    /**
     * Démarre le pulsateur
     */
    public void start(){
        if(!run){
            this.run = true;
            this.thread.start();
        }
    }
    
    /**
     * Stoppe le pulsateur
     */
    public void stop(){
        if(run)
            this.run = false;
    }

    /**
     * Renvoie le hashCode de cet objet {@link Pulsator}
     * @return Retourne le hashCode de cet objet {@link Pulsator}
     */
    @Override
    public final int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    /**
     * Détermine si deux objets {@link Pulsator} sont identiques ou pas
     * @param obj Correspond au second objet {@link Pulsator} à comparer au courant
     * @return Retourne true s'ils sont identiques, sinon false
     */
    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pulsator other = (Pulsator) obj;
        return this.id == other.id;
    }

    /**
     * Renvoie l'objet {@link Pulsator} sous la forme d'une chaîne de caractères
     * @return Retourne l'objet {@link Pulsator} sous la forme d'une chaîne de caractères
     */
    @Override
    public final String toString() {
        return getClass().getSimpleName() + "{" + "id=" + id + ", durationTick=" + durationTick + ", countLoopByTick=" + countLoopByTick + ", run=" + run + '}';
    }

    /**
     * Compare deux objets {@link Pulsator}
     * @param o Correspond au second objet {@link Pulsator} à comparer au courant
     * @return Retourne le résultat de la comparaison
     */
    @Override
    public final int compareTo(Pulsator o) {
        if(o == null) 
            return 1;
        if(this.id < o.id) 
            return -1;
        else if(this.id > o.id) 
            return 1;
        else 
            return 0;
    }
    
    
    
//METHODE ABSTRACT
    /**
     * Lors d'un nouveau tick
     * @param cpt Correspond au numéro du tick
     * @param tps Correspond au nombre de tick par seconde. Avant la permière seconde, ce nombre n'a pas été déterminé. C'est pourquoi il est à -1
     */
    protected abstract void tick(long cpt, int tps);
    
    
    
//METHODE PRIVATE
    /**
     * Renvoie l'objet {@link Runnable} créé spécialement pour générer des pulsations
     * @return Retourne l'objet {@link Runnable} créé spécialement pour générer des pulsations
     */
    @SuppressWarnings("SleepWhileInLoop")
    private Runnable createRunnable(){
        return () -> {
            try {
                java.util.Date startDate = new java.util.Date();
                int  tps        = -1;
                long oldR1      = 0;
                long oldR2      = 0;
                long cpt        = 0;
                long cptBySec   = 0;
                long msOffset   = 0;
                long tickTime   = countLoopByTick;
                while (this.run) {
                    java.util.Date currentDate = new java.util.Date();
                    long interval = currentDate.getTime() - startDate.getTime();
                    if(msOffset == 0)
                        Thread.sleep(tickTime);
                    else{
                        long diff = countLoopByTick - msOffset;
                        if(diff <= 0){
                            msOffset--;
                            Thread.sleep(countLoopByTick - 1);
                        }else{
                            Thread.sleep(diff);
                            msOffset = 0;
                        }
                    }
                    long r1 = interval % durationTick;
                    long r2 = interval % 1000;
                    if(r2 < oldR2){
                        tps         = (int)cptBySec;
                        cptBySec    = 0;
                    }
                    if(r1 < oldR1){
                        cptBySec++;
                        cpt++;
                        tick(cpt, tps);
                    }
                    oldR1 = r1;
                    oldR2 = r2;
                    java.util.Date endLoop  = new java.util.Date();
                    long durationLoop       = endLoop.getTime() - currentDate.getTime();
                    long msTooMushByLoop    = durationLoop - countLoopByTick;
                    if(msTooMushByLoop <= 0)
                        tickTime = countLoopByTick;
                    else{
                        long d = countLoopByTick - msTooMushByLoop;
                        if(d <= 0){
                            msOffset += Math.abs(d) + 1;
                            tickTime = 1;
                        }else
                            tickTime = countLoopByTick - msTooMushByLoop;
                    }
                }
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        };
    }
    
    
    
}