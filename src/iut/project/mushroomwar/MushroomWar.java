/*
 * Copyright (C) 2022 IUT
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package iut.project.mushroomwar;



/**
 * Cette classe permet de lancer l'application
 * @author Joseph BRIGUET
 * @version 1.0
 */
public final class MushroomWar {
    
    
    
//ATTRIBUT STATIC
    /**
     * Correspond au look and feel de l'application
     */
    private final static String LOOK_AND_FEEL = "Windows";

    
    
//CONSTRUCTOR
    /**
     * Correspond au constructeur par défaut
     * @deprecated Ne pas utiliser
     */
    @Deprecated
    private MushroomWar() {
    }

    
    
//MAIN
    /**
     * Lance l'application
     * @param args Correspond aux éventuels arguments
     */
    public final static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if (LOOK_AND_FEEL.equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MushroomWar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(() -> {
            new Screen().setVisible(true);
        });
    }
    
    
    
}